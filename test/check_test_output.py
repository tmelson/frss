#!/usr/bin/env python3

# This file is part of FRSS.
#
# FRSS is free software: you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRSS is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with FRSS.  If not, see <http://www.gnu.org/licenses/>.
import sys
import csv
from io import StringIO

def read_data():
    lines = sys.stdin.readlines()
    for line in lines:
        print(line.rstrip('\n'))
    lines = list(filter(lambda line: '[FRSS]' in line, lines))

    data = {}
    f = StringIO(''.join(lines))
    reader = csv.reader(f, delimiter=' ')
    for row in reader:
        line = {}
        if row[3].startswith('MPI_Pcontrol'):
            assert(row[11] == 'RSS:')
            line['RSS'] = convert_number(row[12])
            assert(row[15] == 'HWM:')
            line['HWM'] = convert_number(row[16])
            assert(row[19] == 'dt:')
            line['dt'] = convert_number(row[20])
            rank = int(row[8])
            if not rank in data:
                data[rank] = [line]
            else:
                data[rank].append(line)
        elif row[3].startswith('End'):
            assert(row[11] == 'HWM:')
            line['HWM'] = convert_number(row[12])
            assert(row[15] == 'dt:')
            line['dt'] = convert_number(row[16])
            rank = int(row[8])
            if not rank in data:
                data[rank] = [line]
            else:
                data[rank].append(line)
    return data

def convert_number(value):
    if value.endswith('us'):
        return int(value[:-2])
    elif value.endswith('ms'):
        return int(value[:-2]) * 1e3
    elif value.endswith('s'):
        return int(value[:-1]) * 1e6
    elif value.endswith('K'):
        return int(value[:-1])
    elif value.endswith('M'):
        return int(value[:-1]) * 1024
    elif value.endswith('G'):
        return int(value[:-1]) * 1024**2
    return int(value)

def assert_approx_equal(value1, value2):
    if value1 == 0:
        if value2 != 0:
            print("Values differ:", value1, value2)
            assert(False)
    elif abs(value2 - value1) / value1 > 0.05:
        print("Values differ by more than 5%:", value1, value2, abs(value2 - value1) / value1)
        assert(False)


mpi_ranks = int(sys.argv[1])

data = read_data()

assert(len(data) == mpi_ranks)
for rank in range(0,mpi_ranks):
    assert(rank in data)
    assert(len(data[rank]) == 4)

    assert(data[rank][0]['RSS'] < 4096)
    assert(data[rank][0]['HWM'] < 4096)
    assert_approx_equal(data[rank][1]['RSS'], data[rank][1]['HWM'])
    assert_approx_equal(data[rank][1]['RSS'], data[rank][0]['RSS'] + 102400)
    assert_approx_equal(data[rank][2]['HWM'], data[rank][1]['HWM'])
    assert_approx_equal(data[rank][3]['HWM'], data[rank][1]['HWM'])

    for i in range(4):
        assert(data[rank][i]['dt'] > 0)
    assert(data[rank][2]['dt'] > 900000)
