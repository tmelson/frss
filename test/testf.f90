! This file is part of FRSS.
!
! FRSS is free software: you can redistribute it and/or modify it
! under the terms of the GNU Lesser General Public License as published
! by the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! FRSS is distributed in the hope that it will be useful, but
! WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
! GNU Lesser General Public License for more details.
!
! You should have received a copy of the GNU Lesser General Public
! License along with FRSS.  If not, see <http://www.gnu.org/licenses/>.

program test
    use, intrinsic :: iso_fortran_env, only: real64
    use mpi

    implicit none
    integer :: ierr
    real(real64), allocatable :: array(:)

    call mpi_init(ierr)

    call mpi_pcontrol(0)

    ! 100M array
    allocate(array(13107200))
    array(:) = 0.d0

    call mpi_pcontrol(1)

    call sleep(1)

    deallocate(array)

    call mpi_pcontrol(2)

    call mpi_finalize(ierr)
end program
