/* This file is part of FRSS.
 *
 * FRSS is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRSS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FRSS.  If not, see <http://www.gnu.org/licenses/>.
 */
#define _GNU_SOURCE
#include <assert.h>
#include <unistd.h>
#include "frss.c"

void test_frss_read_value() {
    assert(frss_read_value("/proc/self/status", "VmRSS: %ld kB") > 0L);
    assert(frss_read_value("/proc/self/status", "not found %ld") == 0L);
    assert(frss_read_value("invalid_file", "not found %ld") == 0L);
}

void test_frss_get_rank() {
    assert(frss_get_rank() < 0);
}

void test_frss_get_dt() {
    assert(frss_get_dt() == 0L);
    sleep(1);
    assert(frss_get_dt() > 900000L);
}

void test_frss_get_mem_unit() {
    long value = 0L;
    assert(frss_get_mem_unit(&value) == 'K');
    assert(value == 0L);
    value = 10L;
    assert(frss_get_mem_unit(&value) == 'K');
    assert(value == 10L);
    value = 20480L;
    assert(frss_get_mem_unit(&value) == 'M');
    assert(value == 20L);
    value = 44040192L;
    assert(frss_get_mem_unit(&value) == 'G');
    assert(value == 42L);
}

void test_frss_get_time_unit() {
    long value = 0L;
    char unit[3];
    frss_get_time_unit(&value, unit);
    assert(unit[0] == 's');
    assert(value == 0L);
    value = 10L;
    frss_get_time_unit(&value, unit);
    assert(unit[0] == 'u');
    assert(value == 10L);
    value = 20000L;
    frss_get_time_unit(&value, unit);
    assert(unit[0] == 'm');
    assert(value == 20L);
    value = 42000000L;
    frss_get_time_unit(&value, unit);
    assert(unit[0] == 's');
    assert(value == 42L);
}

int main() {
    test_frss_read_value();
    test_frss_get_rank();
    test_frss_get_dt();
    test_frss_get_mem_unit();
    test_frss_get_time_unit();
    return 0;
}
