/* This file is part of FRSS.
 *
 * FRSS is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRSS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FRSS.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <mpi.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    MPI_Init(&argc, &argv);

    MPI_Pcontrol(0);

    // 100M array
    const int size = 13107200;
    double* ptr = calloc(size, sizeof(double));
    for (int i = 0; i < size; i++) {
        ptr[i] = i;
    }

    MPI_Pcontrol(1);

    sleep(1);

    free(ptr);

    MPI_Pcontrol(2);

    MPI_Finalize();
    return 0;
}
