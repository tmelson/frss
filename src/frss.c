/* This file is part of FRSS.
 *
 * FRSS is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRSS is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with FRSS.  If not, see <http://www.gnu.org/licenses/>.
 */
#define _GNU_SOURCE
#include <mpi.h>
#include <dlfcn.h>
#include <stdio.h>
#include <sys/time.h>

long frss_last_time = 0L;
long frss_offset = 0L;
int frss_rank = -1;

long frss_read_value(char *filename, char *pattern) {
    long value = 0L;
    char line[1024];
    FILE *fp = NULL;

    if ((fp = fopen(filename, "r")) != NULL) {
        while (fgets(line, sizeof line, fp)) {
            if (sscanf(line, pattern, &value) == 1) {
                break;
            }
        }
        fclose(fp);
    }

    return value;
}

int frss_get_rank() {
    int rank = -1;
    int initialized = 0;

    const int ierr = MPI_Initialized(&initialized);
    if (ierr == 0 && initialized == 1) {
        MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    }

    return rank;
}

long frss_get_dt() {
    struct timeval tv;

    if (gettimeofday(&tv, NULL) != 0) {
        return 0L;
    }

    const long t = (long) (tv.tv_sec) * 1000000L + (long) (tv.tv_usec);
    long dt = 0L;

    if (frss_last_time > 0L && t > frss_last_time) {
        dt = t - frss_last_time;
    }
    frss_last_time = t;
    return dt;
}

char frss_get_mem_unit(long *value) {
    const long M = 1024L;
    const long G = 1024L * 1024L;

    if (*value > 10 * G) {
        *value /= G;
        return 'G';
    }
    if (*value > 10 * M) {
        *value /= M;
        return 'M';
    }
    return 'K';
}

void frss_get_time_unit(long *value, char unit[3]) {
    const long s = 1000000L;
    const long ms = 1000L;

    if (*value > 10 * s || *value == 0L) {
        *value /= s;
        sprintf(unit, "s ");
        return;
    }
    if (*value > 10 * ms) {
        *value /= ms;
        sprintf(unit, "ms");
        return;
    }
    sprintf(unit, "us");
}

void frss_print_pcontrol(int level) {
    long rss = frss_read_value("/proc/self/status", "VmRSS: %ld kB") - frss_offset;
    long hwm = frss_read_value("/proc/self/status", "VmHWM: %ld kB") - frss_offset;
    long dt = frss_get_dt();

    const char rss_unit = frss_get_mem_unit(&rss);
    const char hwm_unit = frss_get_mem_unit(&hwm);
    char dt_unit[3];
    frss_get_time_unit(&dt, dt_unit);

    char output[256];
    sprintf(output, "[FRSS]   MPI_Pcontrol(%d)   MPI rank: %d   RSS: %ld%c   HWM: %ld%c   dt: %ld%s\n",
            level, frss_rank, rss, rss_unit, hwm, hwm_unit, dt, dt_unit);
    fputs(output, stderr);
}

void frss_print_uninit() {
    long hwm = frss_read_value("/proc/self/status", "VmHWM: %ld kB") - frss_offset;
    long dt = frss_get_dt();

    const char hwm_unit = frss_get_mem_unit(&hwm);
    char dt_unit[3];
    frss_get_time_unit(&dt, dt_unit);

    char output[256];
    sprintf(output, "[FRSS]   End   MPI rank: %d   HWM: %ld%c   dt: %ld%s\n", frss_rank, hwm, hwm_unit, dt, dt_unit);
    fputs(output, stderr);
}

void frss_set_offset() {
    frss_offset = frss_read_value("/proc/self/status", "VmRSS: %ld kB");
    frss_rank = frss_get_rank();
    frss_get_dt();
}

static int (*intercepted_MPI_Init)(int *argc, char ***argv);
__attribute__ ((visibility ("default")))
int MPI_Init(int *argc, char ***argv) {
    int res = 1;
    if (intercepted_MPI_Init != NULL) {
        res = intercepted_MPI_Init(argc, argv);
    }
    frss_set_offset();
    return res;
}

static int (*intercepted_PMPI_Init)(int *argc, char ***argv);
__attribute__ ((visibility ("default")))
int PMPI_Init(int *argc, char ***argv) {
    int res = 1;
    if (intercepted_PMPI_Init != NULL) {
        res = intercepted_PMPI_Init(argc, argv);
    }
    frss_set_offset();
    return res;
}

static int (*intercepted_MPI_Init_thread)(int *argc, char ***argv, int required, int *provided);
__attribute__ ((visibility ("default")))
int MPI_Init_thread(int *argc, char ***argv, int required, int *provided) {
    int res = 1;
    if (intercepted_MPI_Init_thread != NULL) {
        res = intercepted_MPI_Init_thread(argc, argv, required, provided);
    }
    frss_set_offset();
    return res;
}

static int (*intercepted_PMPI_Init_thread)(int *argc, char ***argv, int required, int *provided);
__attribute__ ((visibility ("default")))
int PMPI_Init_thread(int *argc, char ***argv, int required, int *provided) {
    int res = 1;
    if (intercepted_PMPI_Init_thread != NULL) {
        res = intercepted_PMPI_Init_thread(argc, argv, required, provided);
    }
    frss_set_offset();
    return res;
}

static int (*intercepted_MPI_Pcontrol)(const int level, ...);
__attribute__ ((visibility ("default")))
int MPI_Pcontrol(const int level, ...) {
    frss_print_pcontrol(level);
    if (intercepted_MPI_Pcontrol != NULL) {
        return intercepted_MPI_Pcontrol(level);
    }
    return 1;
}

static int (*intercepted_PMPI_Pcontrol)(const int level, ...);
__attribute__ ((visibility ("default")))
int PMPI_Pcontrol(const int level, ...) {
    frss_print_pcontrol(level);
    if (intercepted_PMPI_Pcontrol != NULL) {
        return intercepted_PMPI_Pcontrol(level);
    }
    return 1;
}

__attribute__ ((constructor))
void frss_init() {
    intercepted_MPI_Init = dlsym(RTLD_NEXT, "MPI_Init");
    intercepted_PMPI_Init = dlsym(RTLD_NEXT, "PMPI_Init");
    intercepted_MPI_Init_thread = dlsym(RTLD_NEXT, "MPI_Init_thread");
    intercepted_PMPI_Init_thread = dlsym(RTLD_NEXT, "PMPI_Init_thread");
    intercepted_MPI_Pcontrol = dlsym(RTLD_NEXT, "MPI_Pcontrol");
    intercepted_PMPI_Pcontrol = dlsym(RTLD_NEXT, "PMPI_Pcontrol");
}

__attribute__ ((destructor))
void frss_uninit() {
    if (frss_rank >= 0) {
        frss_print_uninit();
    }
}
