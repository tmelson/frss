# FRSS

## Introduction

FRSS is a library for printing the resident set size (RSS) of a Fortran, C, or C++ application at runtime.
It is MPI-aware and displays the memory information for the corresponding MPI rank.
The library intercepts all function calls to `MPI_Init`, `MPI_Init_thread`, and `MPI_Pcontrol`.
An initial measurement of the RSS is triggered by `MPI_Init`/`MPI_Init_thread` to normalize all following measurements
to this value. After that, all calls to `MPI_Pcontrol` generate an output of the measured RSS and its high water mark.

## Usage

### Print the RSS of an MPI task

To trigger the output for your application, load the FRSS library with:
```Fortran
export LD_PRELOAD=/some/path/libfrss.so
```
Here, `/some/path` is the installation directory of the library.
Please do not link your application with it.

Then, in Fortran, call the subroutine:
```Fortran
call MPI_Pcontrol(0)
```
In your C or C++ application, call:
```C
MPI_Pcontrol(0);
```
You can pass any integer number (instead of `0`) to distinguish the calls.

FRSS will print the following information to stderr:

| Abbreviation | Description                                                                                   | Source                         |
| ------------ | --------------------------------------------------------------------------------------------- | ------------------------------ |
| RSS          | Current resident set size normalized to the value measured after `MPI_Init`/`MPI_Init_thread` | `VmRSS` in `/proc/self/status` |
| HWM          | High water mark of the RSS                                                                    | `VmHWM` in `/proc/self/status` |
| dt           | Elapsed wall-clock time since last call of `MPI_Init`, `MPI_Init_thread`, or `MPI_Pcontrol`   |                                |

`MPI_Pcontrol` is part of the MPI standard. If you want to disable FRSS again, you do not need to modify and recompile your code. Just remove
it with `unset LD_PRELOAD`.

## Build and Install

Run `./autogen.sh` if the file `configure` does not exist. Then, run `./configure`, `make`, and `make install`. You can overwrite the installation
directory with `./configure --prefix=/some/path`. See `./configure --help` for details.

## License

[LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.html)
